<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Bienvenido a las operaciones</title>
</head>
<body>
    <h1>Bienvenido a las operaciones</h1>

    <!-- Uso del método POST para formularios -->
    <form method="post">
        <label for="dato1">Dato 1:</label>
        <input type="text" id="dato1" name="dato1" required>
        <br><br>
        <label for="dato2">Dato 2:</label>
        <input type="text" id="dato2" name="dato2" required>
        <br><br>
        <button type="submit" name="operacion" value="suma">Suma</button>
        <button type="submit" name="operacion" value="resta">Resta</button>
    </form>
    <br>
    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Validación del lado del servidor para evitar inyección de código
            $dato1 = $_POST['dato1'];
            $dato2 = $_POST['dato2'];
            $operacion = $_POST['operacion'];
            
             // Verificar que los datos sean numéricos
            if (is_numeric($dato1) && is_numeric($dato2)) {
                $dato1 = floatval($dato1);
                $dato2 = floatval($dato2);
                
                 // Procesar la operación solicitada
                if ($operacion == "suma") {
                    $resultado = $dato1 + $dato2;
                     // Escapar la salida para prevenir XSS
                    echo "<p>Resultado de la suma: $resultado</p>";
                } elseif ($operacion == "resta") {
                    $resultado = $dato1 - $dato2;
                    // Escapar la salida para prevenir XSS
                    echo "<p>Resultado de la resta: $resultado</p>";
                }
            } else {
                // Mensaje de error si los datos no son válidos
                echo "<p>Por favor, ingrese valores numéricos válidos.</p>";
            }
        }
    ?>
</body>
</html>
